//
//  Slider.swift
//  Dashboard1
//
//  Created by Hasani on 3/12/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation
import UIKit

class Slider {
    
    private var description : String?
    private var title : String?
    private var image : String
    private var id : Int
    private var key_id : Int
    private var path : String
    
    
    //Initializer
    init(path : String , description : String? , title : String? , image : String , id : Int , key_id : Int) {
        self.path = path
        self.description = description
        self.title = title
        self.image = image
        self.id = id
        self.key_id = key_id
    }
    
    //setter&getters
    public func setDescription (desc : String?){
        self.description = desc
    }
    public func getDescription () -> String?{
        return description
    }
    public func setTitle (title : String?){
        self.title = title
    }
    public func getTitle () -> String?{
        return title
    }
    public func setImage (img : String){
        self.image = img
    }
    public func getImage() -> String{
        return image
    }
    public func setPath (path : String){
        self.path = path
    }
    public func getPath() -> String{
        return path
    }
    public func setId (id : Int){
        self.id = id
    }
    public func getId () -> Int{
        return id
    }
    public func setKeyId (keyId : Int){
        self.key_id = keyId
    }
    public func getKeyId () -> Int {
        return key_id
    }
    
    //methods
 
}
