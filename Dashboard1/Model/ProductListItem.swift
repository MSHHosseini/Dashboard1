//
//  ProductListItem.swift
//  Dashboard1
//
//  Created by Hasani on 3/16/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation

class ProductListItem {
    
    private var description : String
    private var summary : String
    private var pic : String
    private var like : Int
    private var cost : String
    private var title : String
    
    init(desc : String , summary : String , pic : String , like : Int , cost : String , title : String) {
        self.description = desc
        self.summary = summary
        self.pic = pic
        self.like = like
        self.cost = cost
        self.title = title
    }
    
    public func setDescription (desc : String){
        self.description = desc
    }
    public func getDescription () -> String{
        return description
    }
    public func setLike (like : Int){
        self.like = like
    }
    public func getLike () -> Int {
        return like
    }
    public func setPic (pic : String){
        self.pic = pic
    }
    public func getPic () -> String{
        return pic
    }
    public func setTitle (title : String){
        self.title = title
    }
    public func getTitle () -> String{
        return title
    }
    public func setSummary (summary : String){
        self.summary = summary
    }
    public func getSummary () -> String{
        return summary
    }
    public func setCost (cost : String){
        self.cost = cost
    }
    public func getCost () -> String{
        return cost
    }
    
   
    
    
    
}
