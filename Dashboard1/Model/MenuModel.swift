//
//  MenuModel.swift
//  Dashboard1
//
//  Created by Hasani on 3/22/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation

class MenuModel {
    private var title : String
    private var item : String
    private var type : String
    
    init (title : String , item : String , type : String){
        self.title = title
        self.item = item
        self.type = type
    }
    
    public func getTitle() -> String{
        return title
    }
    public func getItem () -> String{
        return item
    }
    public func getType () -> String{
        return type
    }
    
    
}
