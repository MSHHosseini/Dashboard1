//
//  News.swift
//  Dashboard1
//
//  Created by Hasani on 3/6/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation
class News{
    
    //properties
    private var newsList : [NewsList]
    private var path : String
    private var status : String
    private var thumbnails100 : String
    
    //Initializer
    init(newsList : [NewsList] , path : String , status : String , thumbnails100 : String) {
        self.newsList = newsList
        self.path = path
        self.status = status
        self.thumbnails100 = thumbnails100
    }
    
    //setter&getter
    public func setNewsList (newsList : [NewsList])
    {
        self.newsList = newsList
    }
    public func getNewsList () -> [NewsList]{
        return newsList
    }
    public func setPath (path : String){
        self.path = path
    }
    public func getPath () -> String{
        return path
    }
    public func setStatus (status: String){
        self.status = status
    }
    public func getStatus () -> String {
        return status
    }
    public func setThumbnails100 (thumbnail : String){
        self.thumbnails100 = thumbnail
    }
    public func getThumbnails100 () -> String{
        return thumbnails100
    }
    
   
    
    
    
}
