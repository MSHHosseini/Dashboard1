//
//  MenuTableViewController.swift
//  Dashboard1
//
//  Created by Hasani on 3/8/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import UIKit
import os.log
import Alamofire


class MenuTableViewController: UITableViewController {

    //properties
    var appList : [AppList]?
    {
        didSet{
            performUIUpdatesOnMain {
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.reloadData()
            }
        }
    }
    
    override func awakeFromNib() {
        if appList == nil{
            tableView.dataSource = nil
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                
                self?.getAllProviders(Vc: self!, Domain: Config.getAppProvidersWebService, SuccessHandler: (self?.successed)!, FailureHandler: (self?.failure)!, ErrorHandler: (self?.networkError)!)
            }
        }
        else{
            tableView.dataSource = self
            tableView.delegate = self
            tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if appList == nil{
           tableView.dataSource = nil
        }
         self.tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return (appList?.count)!
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "menuTableCell", for: indexPath) as? MenuTableViewCell else{
              fatalError("The dequeued cell is not an instance of MenuTableViewCell.")
        }
        if appList != nil{
       let app = appList?[indexPath.row]
            if let logo = app?.getLogo(){
               cell.appImage.image = logo
            }
           
        
        cell.appTitle.text = app?.getTitle()
        }
        return cell
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        var destinationViewController=segue.destination
//        if let navigationViewController=destinationViewController as? UINavigationController{
//            destinationViewController=navigationViewController.visibleViewController ?? destinationViewController
//        }
//        }
//        if let mainVc=destinationViewController as? MainViewController ,
//            let selectedApp = sender as? MenuTableViewCell ,
//            let indexpath = tableView.indexPath(for: selectedApp) {
//
//            mainVc.app = appList?[indexpath.row]
//            }
//    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        // Configure the destination view controller only when the save button is pressed.
        

    }
    */
    public func getAllProviders(Vc:UIViewController,Domain:String,SuccessHandler:@escaping (String,String,String)->Void,FailureHandler:@escaping (String)->Void,ErrorHandler:@escaping (NetworkErrors)->Void) -> Void{
        
        if (Usefull.internetIsConnect()){
            
            Usefull.serverRequestManager().request(Domain, method: .post , parameters: nil, encoding: JSONEncoding.default, headers: Usefull.setRequestHeaders()).responseJSON{ response in

                /* GUARD: Did we get a successful 2XX response? */
                if !response.result.isSuccess {
                    
                    FailureHandler("StatusIsNotOk")
                    return
                }
                
                
                guard let data = response.data  else {
                    print("Data Error")
                    return
                }
                // parse the data
                let parsedResult: Config.jsonStandard
                do {
                    parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! Config.jsonStandard
                } catch {
                    print("Could not parse the data as JSON: '\(data)'")
                    return
                }
                print (parsedResult)
                
                guard let stat = parsedResult["status"] as? String, stat == "OK" else {
                    performUIUpdatesOnMain {
                        
                        //self.alert.message = parsedResult["error_msg"] as! String
                       // self.present(self.alert, animated: true, completion: nil)
                        // print("  \(parsedResult[Constants.RegisterResponseValues.errorMessage]!)")
                    }
                    
                    return
                }
                
                
                self.appList = AppList.parseJson(parsedJson: parsedResult)
                
            }
            
            
        }else{
            ErrorHandler(NetworkErrors.INTERNET_DISCONNECTED)
        }
        
    }
    
    //Handlers
    private  func successed(p1:String,p2:String,p3:String) -> Void{
        print(p1+p2+p3)
    }
    private  func failure(Error:String){
        print(Error)
    }
    
    private func networkError(netError:NetworkErrors){
        switch netError{
        case .INTERNET_DISCONNECTED:
            print("No Internet Access.")
        case .SERVER_ERROR:
            print("Server Error.")
            
        }
    }
    

}
