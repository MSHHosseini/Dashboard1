//
//  AppList.swift
//  Dashboard1
//
//  Created by Hasani on 3/6/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation
import UIKit

class AppList{
    
    //Properties
    private var code : String
    private var color : String
    private var cover : String?
    private var domain : URL
    private var domainID : Int
    private var logo : UIImage?
    private var id : Int
    private var lang : String
    private var icon : String?
    private var path : URL
    private var pin : Int
    private var status : Int
    private var title : String
    private var upload : String
    
    //Initializer
    init(code : String , color : String , cover : String? , domain : URL , domainID : Int , logo : UIImage? , id : Int , lang : String , icon : String? , path : URL , pin : Int , status : Int , title : String , upload : String) {
        self.code = code
        self.color = color
        self.cover = cover
        self.domain = domain
        self.domainID = domainID
        self.icon = icon
        self.id = id
        self.lang = lang
        self.logo = logo
        self.path = path
        self.pin = pin
        self.status = status
        self.title = title
        self.upload = upload
    }
    
    //setter&getter
    func setCode (code : String){
        self.code = code
    }
    func getCode () -> String {
        return code
    }
    func setColor (color : String){
        self.color = color
    }
    func getColor () -> String {
        return color
    }
    func setCover (cover : String?){
        self.cover = cover
    }
    func getCover () -> String? {
        return cover
    }
    func setDomain (domain : URL){
        self.domain = domain
    }
    func getDomain () -> URL{
        return domain
    }
    func setDomainID (domainID : Int){
        self.domainID = domainID
    }
    func getDomainID() -> Int {
        return domainID
    }
    func setIcon (icon : String?){
        self.icon = icon
    }
    func getIcon () -> String? {
        return icon
    }
    func setId (id : Int){
        self.id = id
    }
    func getId () -> Int {
        return id
    }
    func setLang (lang : String){
        self.lang = lang
    }
    func getLang () -> String {
        return lang
    }
    func setLogo (logo : UIImage?){
        self.logo = logo
    }
    func getLogo () -> UIImage? {
        return logo
    }
    func setPath (path : URL){
        self.path = path
    }
    func getPath () -> URL {
        return path
    }
    func setPin (pin : Int){
        self.pin = pin
    }
    func getPin () -> Int {
        return pin
    }
    func setStatus(status : Int){
        self.status = status
    }
    func getStatus () -> Int {
        return status
    }
    func setTitle (title : String){
        self.title = title
    }
    func getTitle () -> String {
        return title
    }
    func setUpload (upload : String){
        self.upload = upload
    }
    func getUpload () -> String {
        return upload
    }
    
 
}
