//
//  AppDetails.swift
//  Dashboard1
//
//  Created by Hasani on 3/12/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation
import UIKit

class AppDetails {
    
   // private var contacts : Contacts
    private var news : News
    private var slider : [Slider]
    private var path : String
    private var products : Products
    private var menu : [MenuModel]
    
    //Initializer

    init(path : String , news : News , slider : [Slider] , products : Products , menu : [MenuModel] ) {
            self.path = path
            self.news = news
            self.slider = slider
        self.products = products
        self.menu = menu
        }
    
    //setter&getter
//    public func setContacts (contacts : Contacts){
//        self.contacts = contacts
//    }
//    public func getContatcts () -> Contacts{
//        return contacts
//    }
    public func setNews (news : News){
        self.news = news
    }
    public func getNews() -> News{
        return news
    }
    public func setProducts (products : Products){
        self.products = products
    }
    public func getProducts() -> Products{
        return products
    }
    public func setSlider (slider : [Slider]){
        self.slider = slider
    }
    public func getSlider () -> [Slider]{
        return slider
    }
    public func getPath () -> String{
        return path
    }
    public func setPath (path : String){
        self.path = path
    }
    public func getMenu () -> [MenuModel]{
        return menu
    }
    public func setMenu (menu : [MenuModel]){
        self.menu = menu
    }
    //methods

    
}
