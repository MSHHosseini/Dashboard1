//
//  NewsList.swift
//  Dashboard1
//
//  Created by Hasani on 3/6/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation
class NewsList{
   
    //properties
    private var comments : Int
    private var created_at : String
    private var description : String
    private var id : Int
    private var like : Int
    private var statistics : String?
    private var summary : String
    private var title : String
    private var updated_at : String
    private var pic : String
    
    //initializer
    init(comments : Int , created_at : String , description : String , id : Int , like : Int , statistics : String? , summary : String , title : String , updated_at : String , pic : String) {
        self.comments = comments
        self.created_at = created_at
        self.description = description
        self.id = id
        self.like = like
        self.statistics = statistics
        self.summary = summary
        self.title = title
        self.updated_at = updated_at
        self.pic = pic
        
    }
    
    //setter&getter
    public func setComments (comments : Int){
        self.comments = comments
    }
    public func getComments () -> Int{
        return comments
    }
    public func setCreated (created : String){
        self.created_at = created
    }
    public func getCreated () -> String{
        return created_at
    }
    public func setDescription (desc : String){
        self.description = desc
    }
    public func getDescription () -> String{
        return description
    }
    public func setPic (pic : String){
        self.pic = pic
    }
    public func getpic () -> String{
        return pic
    }
    public func setId (id : Int){
        self.id = id
    }
    public func getId () -> Int {
        return id
    }
    public func setLike (like : Int){
        self.like = like
    }
    public func getLike () -> Int{
        return like
    }
    public func setStatistics (statis : String?){
        self.statistics = statis
    }
    public func getStatistics () -> String?{
        return statistics
    }
    public func setSummary (summary : String){
        self.summary = summary
    }
    public func getSummary () -> String{
        return summary
    }
    public func setTitle (title : String){
        self.title = title
    }
    public func getTitle () -> String{
        return title
    }
    public func setUpdatedAt (updtat : String){
        self.updated_at = updtat
    }
    public func getUpdatedAt () -> String{
        return updated_at
    }
   
    
}
