//
//  MainControllers.swift
//  Dashboard1
//
//  Created by Hasani on 3/20/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation
import Alamofire

class MainController {
    

    static var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE1NzU1MCwiaXNzIjoiaHR0cHM6Ly9hcHBzaXRlLm5ldC9yZXN0LWFwaS8xL2xvZ2luIiwiaWF0IjoxNTI2ODAyMjQ3LCJleHAiOjE1NTgzMzgyNDcsIm5iZiI6MTUyNjgwMjI0NywianRpIjoiOEE4ajVWcTN4a2RqUFFHYiJ9._iTC4HSGlqraReGQgPY1M_OyX7i4bOrdvSKu6Vjne3k"
    
    //get Specified Add's Details
    
    public static func getAppDetails(Vc:UIViewController,Domain:String, appID : Int , SuccessHandler:@escaping ( ([AppList] ,  AppDetails))->Void,FailureHandler:@escaping (String)->Void,ErrorHandler:@escaping (NetworkErrors)->Void) -> Void{
        
        if (Usefull.internetIsConnect()){
            let params = ["token" : token , "dashboard" : 1 , "club" : appID , "pin" : 1] as [String : Any]
            
            Usefull.serverRequestManager().request(Domain, method: .post , parameters: params , encoding: URLEncoding(destination: .queryString), headers: Usefull.setRequestHeaders()).responseJSON{ response in
                
                
                /* GUARD: Did we get a successful 2XX response? */
                if !response.result.isSuccess {
                    
                    FailureHandler("مشکل در دریافت اطلاعات")
                    return
                }
            
                
                guard let data = response.data  else {
                    print("Data Error")
                    return
                }
                
                
                
                // parse the data
                let parsedResult: Config.jsonStandard
                do {
                    parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! Config.jsonStandard
                } catch {
                    print("Could not parse the data as JSON: '\(data)'")
                    return
                }
                
                
                guard let stat = parsedResult["status"] as? String, stat == "OK" else {
                    if let error = parsedResult["error_msg"] as? String{
                        FailureHandler(error)
                    }
                    return
                }
               // print(parsedResult)
                
                let touple = parseJson(parsedJson: parsedResult, appID: appID)
                    
                    SuccessHandler( touple )
                    
                
            }
                
                
            }else{
                ErrorHandler(NetworkErrors.INTERNET_DISCONNECTED)
            }
            
        }
    static func parseJson (parsedJson : Config.jsonStandard , appID : Int) -> ([AppList] , AppDetails){
            var appArray = [AppList]()
        var appDetails : AppDetails!
            let arrayElement = parsedJson["clubs"] as! [Config.jsonStandard]
            if arrayElement.count == 0{
                print("NO App...")
            }
            else{
                for element in arrayElement{
                
                    let code = element[Constants.AppListResponseKeys.code] as! String
                    let color = element[Constants.AppListResponseKeys.color] as! String
                    let cover = element[Constants.AppListResponseKeys.cover] as? String
                    let domain = element[Constants.AppListResponseKeys.domain] as! String
                    let domain_id = element[Constants.AppListResponseKeys.domain_id] as! Int
                    let iconName = element[Constants.AppListResponseKeys.icon] as? String
                    let id = element[Constants.AppListResponseKeys.id] as! Int
                    let lang = element[Constants.AppListResponseKeys.lang] as! String
                    let path = element[Constants.AppListResponseKeys.path] as! String
                    let logoName = element[Constants.AppListResponseKeys.logo] as? String
                    
                    let pin = Int(element[Constants.AppListResponseKeys.pin] as! String)!
                    let status = Int(element[Constants.AppListResponseKeys.status] as! String)!
                    let title = element[Constants.AppListResponseKeys.title] as! String
                    let upload = element[Constants.AppListResponseKeys.upload] as! String
                    if logoName != nil{
                        let url = path+"/"+upload+"/"+logoName!
                        
                        let dataLogo = try? Data(contentsOf: URL(string : url )!)
                        if let datalogoNotNull = dataLogo{
                            let logoImg = UIImage(data : datalogoNotNull)
                            
                            let app = AppList(code: code, color: color, cover: cover, domain: URL(string : domain)!, domainID: domain_id, logo: logoImg, id: id, lang: lang, icon: iconName, path: URL(string : path)!, pin: pin, status: status, title: title, upload: upload)
                            appArray.append(app)
                        }
                    }
                    else {
                        let app = AppList(code: code, color: color, cover: cover, domain: URL(string : domain)!, domainID: domain_id, logo: nil, id: id, lang: lang, icon: iconName, path: URL(string : path)!, pin: pin, status: status, title: title, upload: upload)
                        appArray.append(app)
                    }
                    
                    
                    if  id == appID{
                        var menuItems = [MenuModel] ()
                        let app = element["app"] as! [Config.jsonStandard]
                        
                        let menu = app[0]["menu"] as! [Config.jsonStandard]
                        for items in menu{
                            let item = items["item"] as! String
                            let title = items["title"] as! String
                            let type = items["type"] as! String
                            let menuItem = MenuModel(title: title, item: item, type: type)
                            menuItems.append(menuItem)
                        }
                        
                        
                        let sliderDic = element["slider"] as! Config.jsonStandard
                        let slider = parseSliderJson(sliderDic: sliderDic)
                        let newsDic = element["news"] as! Config.jsonStandard
                        let news = parseJsonToNews(newsDic: newsDic)
                        let productDic = element["product"] as! Config.jsonStandard
                        let products = parseJsonToProducts(productDic: productDic)
                        let path = element["path"] as! String
                        // let logoName = items["logo"] as! String
                        // let title = items["title"] as! String
                        appDetails = AppDetails(path : path , news: news, slider: slider , products : products, menu: menuItems )
                    }
                    
                }
            }
        return (appArray , appDetails)
            
        }
        
        
    public static func parseSliderJson(sliderDic : Config.jsonStandard ) -> [Slider]{
        var sliders = [Slider]()
        
        
        let sliderList = sliderDic["list"] as! [Config.jsonStandard]
        let path = sliderDic["path"] as! String
        //"https://gold.appsite.net/assets/upload/242/club/"
        for item in sliderList {
            let description = item["description"] as? String
            let title = item["title"] as? String
            let imageName = item["file"] as! String
            let id = item["id"] as! Int
            let key_id = item["key_id"] as! Int
            let slider = Slider(path : path , description : description, title: title, image: imageName, id: id, key_id: key_id)
            sliders.append(slider)
            // }
            
            
            //}
        }
        return sliders
    }
        
        
    public static func parseJsonToNews(newsDic : Config.jsonStandard) -> News{
        let news : News!
        let list = newsDic["list"] as! [Config.jsonStandard]
        let newsList = parseNewsListJson(newsListDic: list)
        let path = newsDic["path"] as! String
        let status = newsDic["status"] as! String
        let thumbnails = newsDic["thumbnails"] as! Config.jsonStandard
        let thumbnails100 = thumbnails["thumbnail100"] as! String
        
        news = News(newsList: newsList, path: path , status: status, thumbnails100: thumbnails100)
        
        return news
    }
    
    public static func parseNewsListJson (newsListDic : [Config.jsonStandard]) -> [NewsList]{
        var newsArray = [NewsList]()
        for item in newsListDic{
            let comments = item["comments"] as! Int
            let createdAt = item["created_at"] as! String
            let description = item["description"] as! String
            let id = item ["id"] as! Int
            let like = item["like"] as! Int
            let statistics = item["statistics"] as? String
            let summary = item["summary"] as! String
            let title = item["title"] as! String
            let updated_at = item["updated_at"] as! String
            let pic = item["pic"] as! String
            let news = NewsList(comments: comments, created_at: createdAt, description: description, id: id, like: like, statistics: statistics, summary: summary, title: title, updated_at: updated_at , pic : pic)
            newsArray.append(news)
            
            
        }
        return newsArray
        
        
    }
    
    public static func parseJsonToProducts(productDic : Config.jsonStandard) -> Products{
        let products : Products!
        let list = productDic["list"] as! [Config.jsonStandard]
        let productsList = parseProductsList(productsListDic: list)
        let path = productDic["path"] as! String
        
        products = Products(list: productsList, path: path)
        
        
        return products
    }
    
    public static func parseProductsList (productsListDic : [Config.jsonStandard]) -> [ProductListItem]{
        var productsArray = [ProductListItem]()
        for item in productsListDic{
            
            let description = item["description"] as! String
            let like = item ["like"] as! Int
            let pic = item["pic"] as! String
            let summary = item["summary"] as! String
            let cost = item["cost"] as AnyObject
            let title = item["title"] as! String
            
            let product = ProductListItem(desc: description, summary: summary, pic: pic, like: like, cost: String(describing: cost) , title : title)
            productsArray.append(product)
            
            
        }
        return productsArray
        
        
    }
    
    
    
        
}
