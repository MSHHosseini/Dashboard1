//
//  MenuViewController.swift
//  Dashboard1
//
//  Created by Hasani on 3/18/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import UIKit
import os.log
import Alamofire


class MenuViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    
    
    @IBAction func searchtable(_ sender: UIButton) {
    }
    @IBOutlet weak var tableView: UITableView!
  //  let searchController = UISearchController(searchResultsController: nil)
    @IBAction func searchAppList(_ sender: UIButton) {
    }
    
    //properties
    var appList : [AppList]?
    {
        didSet{
            
            performUIUpdatesOnMain {
                self.tableView.dataSource = self
                self.tableView.delegate = self
                self.tableView.reloadData()
            }
        }
    }
    
    override func awakeFromNib() {
//        if appList == nil{
//
//            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
//
//                self?.getAllProviders(Vc: self!, Domain: Config.getAppProvidersWebService, SuccessHandler: (self?.successed)!, FailureHandler: (self?.failure)!, ErrorHandler: (self?.networkError)!)
//            }
//        }
//        else{
//            tableView.dataSource = self
//            tableView.delegate = self
//            tableView.reloadData()
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // searchController.searchResultsUpdater = self
        //searchController.dimsBackgroundDuringPresentation = false
       // definesPresentationContext = true
       // tableView.tableHeaderView = searchController.searchBar
        self.tableView.tableFooterView = UIView()
    }
    

    // MARK: - Table view data source
    
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if appList == nil{
            return 0
        }
        else {
            return (appList?.count)!
            
        }
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "menuTableCell", for: indexPath) as? MenuTableViewCell else{
            fatalError("The dequeued cell is not an instance of MenuTableViewCell.")
        }
  if appList != nil{
            let app = appList?[indexPath.row]
            if let logo = app?.getLogo(){
                cell.appImage.image = logo
            }
            
            
            cell.appTitle.text = app?.getTitle()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)

    }
    

    
    
    

}

