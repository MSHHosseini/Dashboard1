//
//  NetworkErrors.swift
//  appsite
//
//  Created by Steveirani.
//  Copyright © 2017 farasadr. All rights reserved.
//

import UIKit
enum NetworkErrors{
    case INTERNET_DISCONNECTED
    case SERVER_ERROR
}

