//
//  MainViewController.swift
//  Dashboard1
//
//  Created by Hasani on 3/5/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import UIKit
import Alamofire
import Auk

class MainViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource{
    
    
    //properties
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    @IBOutlet weak var productCollectionView: UICollectionView!
    
    @IBOutlet weak var appTitle: UILabel!
    @IBOutlet weak var Logo: UIImageView!
    @IBOutlet weak var newsCollectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!

    var alertMessage = ""
    var alert : UIAlertController!
    var appList : [AppList]?
    var app : AppList!{
        didSet{
            navigationItem.title = app.getTitle()
            appTitle.text = app.getTitle()
            Logo.image = app.getLogo()
            
        }
    }

    var appDetails : AppDetails?{
        didSet{

            if appDetails != nil {
                self.scrollView.auk.removeAll()
            let slider = appDetails!.getSlider()
            for item in slider{
                let urlStr = item.getPath()+String(item.getKeyId())+"/"+item.getImage()
                performUIUpdatesOnMain {
                  self.scrollView.auk.show(url: urlStr)
                    self.scrollView.auk.startAutoScroll(delaySeconds: 2.0)
                }
            }
                
                   self.productCollectionView.dataSource = self
                   self.productCollectionView.delegate = self
                   self.productCollectionView.reloadSections(IndexSet(integer : 0))
                
                    self.newsCollectionView.dataSource = self
                    self.newsCollectionView.delegate = self
                   self.newsCollectionView.reloadSections(IndexSet(integer : 0))
                   self.spinner.stopAnimating()


            
        }
    }
    }
    
   
 
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        navigationItem.title = "Appsite"
        // Initialize Tab Bar Item
        tabBarItem = UITabBarItem(title: "صفحه اصلی" , image: UIImage(named: "home"), tag: 0)
        
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        alert = UIAlertController(title: "Message", message: self.alertMessage, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        spinner.startAnimating()
        spinner.hidesWhenStopped = true
        var appID = UserDefaults.standard.integer(forKey: "appID")
        if appID == 0 {
            appID = 232
        }
        MainController.getAppDetails(Vc: self, Domain: Config.Domain+routes.PROVIDER_DASHBOARD, appID: appID , SuccessHandler: successed, FailureHandler: failure, ErrorHandler: networkError)
       
        scrollView.auk.settings.pageControl.visible = false
        newsCollectionView.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        productCollectionView.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        

    }

 

    
    
    //newsCollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.newsCollectionView {
            if appDetails != nil{
            return appDetails!.getNews().getNewsList().count
            }
            else
            {
                return 0
            }
        }
        else{
            if appDetails != nil{
        return appDetails!.getProducts().getProductList().count
        }
            else{
                return 0
            }
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        if collectionView == self.newsCollectionView{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newsCell", for: indexPath as IndexPath) as! NewsCollectionViewCell
            if appDetails != nil{
                let news =  appDetails!.getNews().getNewsList()[indexPath.item]
                let picData = try? Data(contentsOf : URL (string : appDetails!.getNews().getPath()+"/"+news.getpic())!)
                if let pic = picData{
                    cell.newsImg.image = UIImage(data: pic)!
                }
                cell.newsSummary.text = news.getTitle()
                cell.newsLike.text = String(news.getSummary())
                
                
            }
            
        return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath as IndexPath) as! NewsCollectionViewCell
            if appDetails != nil{
                let product =  appDetails!.getProducts().getProductList()[indexPath.item]
                let picData = try? Data(contentsOf : URL (string : appDetails!.getProducts().getPath()+"/"+product.getPic())!)
                if let pic = picData{
                cell.newsImg.image = UIImage(data: pic)!
            }
                cell.newsSummary.text = product.getTitle()
                cell.newsLike.text = String(product.getLike())

                
            }
            return cell
        }
    }
    
    @IBAction func makeUnwindSegue(segue:UIStoryboardSegue){
        if let sourceVC = segue.source as? MenuViewController {
            
            
            if let selectedIndexPath = sourceVC.tableView.indexPathForSelectedRow{
                app = sourceVC.appList![selectedIndexPath.row]
                UserDefaults.standard.set(app.getId(), forKey: "appID")
                scrollView.auk.removeAll()
                newsCollectionView.dataSource = nil
                productCollectionView.dataSource = nil
                 self.spinner.startAnimating()
                DispatchQueue.global(qos: .userInitiated).async {
                   
                    MainController.getAppDetails(Vc: self, Domain: Config.Domain+routes.PROVIDER_DASHBOARD, appID: self.app.getId(), SuccessHandler: self.successed, FailureHandler: self.failure, ErrorHandler: self.networkError)
                }

                
            }

        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? UINavigationController ,
            let VC = destinationVC.visibleViewController as? MenuViewController{
            VC.appList = appList
            
        }
        
    }
    
  

    
    
    //Handlers
    private  func successed(touple : ( [AppList] , AppDetails)) -> Void{
        self.appList = touple.0
        for item in touple.0{
            if item.getId() == UserDefaults.standard.integer(forKey: "appID"){
                self.app = item
            }
        }
        self.appDetails = touple.1

       
    }
    private  func failure(Error:String){
        self.alert.message = Error
        self.present(self.alert, animated: true, completion: nil)
    }
    
    private func networkError(netError:NetworkErrors){
        switch netError{
        case .INTERNET_DISCONNECTED:
            self.alert.message = "Internet disconnected"
            self.present(self.alert, animated: true, completion: nil)
        case .SERVER_ERROR:
            self.alert.message = "Server Error"
            self.present(self.alert, animated: true, completion: nil)
            
        }
    }



}


