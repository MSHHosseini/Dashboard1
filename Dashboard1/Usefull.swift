//  Usefull.swift
//  Created by Steve irani on 4/20/17.
//  Copyright © 2017 farasadr. All rights reserved.
//
import UIKit
import Alamofire
class Usefull{

    static func internetIsConnect()->Bool{
        return (NetworkReachabilityManager()?.isReachable)!
    }
    static func serverRequestManager()->Alamofire.SessionManager{
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.session.configuration.timeoutIntervalForResource = 120
        return manager
    }


    static func concateDomain(route:String)->String{
        return Config.Domain+route
    }
    static func getUserToken()->String{
        if UserDefaults.standard.object(forKey: "user_token") != nil {
            return UserDefaults.standard.object(forKey: "user_token") as! String
        }
        return ""
    }
/*
    static func setSystemDefaultLanguage(langModel:languageModel){
        UserDefaults.standard.set(langModel.getLangKeyword(), forKey: "langKey")
        UserDefaults.standard.set(langModel.getIsRtl(), forKey: "langIsRtl")
        UserDefaults.standard.set(langModel.getTitle(), forKey: "langTitle")
        UserDefaults.standard.set(langModel.getIconPath(), forKey: "langIcon")
    }
    static func getDefaultSystemLanguage()->languageModel{
        let langModel=languageModel()
        if(UserDefaults.standard.object(forKey: "langKey") != nil ){
            langModel.setLangKeyword(Value: UserDefaults.standard.string(forKey: "langKey")!)
            langModel.setTitle(Value: UserDefaults.standard.string(forKey: "langTitle")!)
            langModel.setIsRtl(Value: UserDefaults.standard.bool(forKey: "langIsRtl"))
            langModel.setIconBasePath(Value: UserDefaults.standard.string(forKey: "langIcon")!)
        }else{
            langModel.setLangKeyword(Value: "")
        }
        return langModel
    }

*/
    static func setRequestHeaders()->[String:String]{
        let langKey = "fa"
        let headers:[String : String]=["Authorization":"Bearer "+self.getUserToken(),"lang":langKey]
        return headers
    }
  

}
