//
//  Contacts.swift
//  Dashboard1
//
//  Created by Hasani on 3/6/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation

class Contacts {
    //PROPERTIES
    private var address: String
    private var city : Int
    private var country : Int
    private var email : String
    private var facebook : URL
    private var fax : Int
    private var instagram : URL
    private var lat : String
    private var lng : String
    private var mobile : String
    private var ostan : Int
    private var phone : [String]
    private var postcode : Int
    private var telegram : URL
    private var url : URL
    
    //INITIALIZER
    init(address: String , city : Int , country : Int , email : String , facebook : URL , fax : Int , instagram : URL , lat : String , lng : String , mobile : String , ostan : Int , phone : [String] , postcode : Int , telegram : URL , url : URL) {
        self.address = address
        self.city = city
        self.country = country
        self.email = email
        self.facebook = facebook
        self.fax = fax
        self.instagram = instagram
        self.lat = lat
        self.lng = lng
        self.mobile = mobile
        self.ostan = ostan
        self.phone = phone
        self.postcode = postcode
        self.telegram = telegram
        self.url = url
    }
    
    //----SETTER & GETTER
    func setAddress(address : String) {
        self.address = address
    }
    func getAddress() -> String{
        return address
    }
    func setCity(city : Int) {
        self.city = city
    }
    func getCity() -> Int{
        return city
    }
    func setCountry(country : Int) {
        self.country = country
    }
    func getCountry() -> Int{
        return country
    }
    func setEmail(email : String) {
        self.email = email
    }
    func getEmail() -> String{
        return email
    }
    func setFacebook(facebook : URL) {
        self.facebook = facebook
    }
    func getFacebook() -> URL{
        return facebook
    }
    func setFax(fax : Int) {
        self.fax = fax
    }
    func getFax() -> Int{
        return fax
    }
    func setInstagram(instagram : URL) {
        self.instagram = instagram
    }
    func getInstagram() -> URL{
        return instagram
    }
    func setLat(lat : String) {
        self.lat = lat
    }
    func getLat() -> String{
        return lat
    }
    func setLng (lng : String) {
        self.lng = lng
    }
    func getLng() -> String{
        return lng
    }
    func setMobie(mobile : String) {
        self.mobile = mobile
    }
    func getMobile() -> String{
        return mobile
    }
    func setOstan(ostan: Int) {
        self.ostan = ostan
    }
    func getOstan() -> Int{
        return ostan
    }
    func setPhone(phone : [String]) {
        self.phone = phone
    }
    func getPhone() -> [String]{
        return phone
    }
    func setPostcode(postcode: Int) {
        self.postcode = postcode
    }
    func getPostcode() -> Int{
        return postcode
    }
    func setTelegram(telegram : URL) {
        self.telegram = telegram
    }
    func getTelegram() -> URL{
        return telegram
    }
    func setUrl(url : URL) {
        self.url = url
    }
    func getUrl() -> URL{
        return url
    }
    
    /*
    public func parseContactJson (contactDic : Config.jsonStandard) -> Contacts {
        var contacts : Contacts!
        let address = contactDic["address"] as! String
        let city = contactDic["city"] as! Int
        let email = contactDic["email"] as! String
        let country = contactDic["country"] as! Int
        let facebook = contactDic["facebook"] as! String
        let fax = contactDic["fax"] as! Int
        let instagram = contactDic["instagram"] as! String
        let lat = contactDic["lat"] as! String
        let lng = contactDic["lng"] as! String
        let mobile = contactDic["mobile"] as! String
        let ostan = contactDic["ostan"] as! Int
        let phone = contactDic["phone"] as! [String]
        
        
        
        
        return contacts
    }
    
    */
    
}
