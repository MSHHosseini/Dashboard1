//
//  MenuTableViewCell.swift
//  Dashboard1
//
//  Created by Hasani on 3/9/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {


    @IBOutlet weak var appTitle: UILabel!
    
    @IBOutlet weak var appImage: UIImageView!
}
