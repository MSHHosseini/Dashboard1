//
//  Constants.swift
//  AppSiteWebService
//
//  Created by Hasani on 2/7/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation

struct Constants {
    
    
    //Register Response Keys
    struct RegisterParameterseKeys {
        static let first_name = "first_name"
        static let last_name = "last_name"
        static let username = "username"
        static let password = "password"
        static let phone = "phone"
    }
    
    //Register Response Keys
    struct RegisterResponseKeys {
        static let first_name = "first_name"
        static let company_name = "company_name"
        static let last_name = "last_name"
        static let status = "status"
        static let type = "type"
        static let uid = "uid"
        static let username = "username"
    }
    
    // Register Response Values
    struct RegisterResponseValues {
        static let OKStatus = "OK"
        static let errorMessage = "error_msg"
    }
    
    
    //Successful Register Message
static let RegisterSuccess = "کاربر جدید با موفقیت ثبت شد."
    
  
    //AppListResponseKeys
    struct AppListResponseKeys{
        static let code = "code"
        static let color = "color"
        static let cover = "cover"
        static let domain = "domain"
        static let domain_id = "domain_id"
        static let icon = "icon"
        static let id = "id"
        static let lang = "lang"
        static let logo = "logo"
        static let path = "path"
        static let pin = "pin"
        static let status = "status"
        static let title = "title"
        static let upload = "upload"

    }
    
    
    struct LabelTexts{
        static let firstNameLabel = "نام"
        static let lastNameLabel = "نام خانوادگی"
        static let usernameLabel = "نام کاربری"
        static let passwordLabel = "رمز عبور"
        static let phoneLabel = "شماره موبایل"
        static let returnBarButton = "بازگشت"
    }
    
}
