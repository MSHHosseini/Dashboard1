//
//  Products.swift
//  Dashboard1
//
//  Created by Hasani on 3/12/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation
import UIKit

class Products{
    
   private var productsList : [ProductListItem]
    private var path : String
    
    init(list : [ProductListItem] , path: String) {
        self.productsList = list
        self.path = path
    }
    
    public func setProductList (list : [ProductListItem]){
        self.productsList = list
    }
    public func getProductList () -> [ProductListItem]{
        return productsList
    }
    public func setPath (path : String){
        self.path = path
    }
    public func getPath () -> String{
        return path
    }
 
    
    
}
