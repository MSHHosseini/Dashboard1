//  routes.swift
//  Created by Steve irani on 7/8/17.
//  Copyright © 2017 farasadr. All rights reserved.
//

import UIKit
class routes{
    //user
    static let GENERATE_CODE="auth"
    static let CHECK_CODE="check"
    static let Login="login"
    static let REGISTER="register"
    static let isLogin="isLogin"
    static let chgPass="chgpass"
    static let GET_USER_INFO="get-user"
    static let edit_user_detail="profile"
    static let FORGOT_PASS="forgotpassword"
    
    
    //PROVIDER
    static let CHECK_PROVIDER_EXIST="check-domain"
    static let ADD_PROVIDER="user-club"
    static let PROVIDER_DASHBOARD="hologram"
    static let SEARCH_CLUB="search-club"
    static let REGISTER_PROVIDER_REQUEST="send-desktop"
    

    //club
    static let CLUB_ABOUT="get-about"
    static let CLUB_CONTACT="get-contacts"
    static let CONTACTUS="contact-us"
    static let GOOGLE_MAP_URL="contactus"
    static let CLUB_SETTING="get-setting"

    
    //news
    static let NEWS_LIST="get-news-list"
    static let NEWS_LIKE="add-news-rate"
    static let ADD_NEWS_COMMENT="add-news-comment"
    
    //score
    static let GET_SCORE="get-score"
    
    //service
    static let SERVICE_CAT_LIST="get-service-category-list"
    static let SERVICE_LIST="get-service-list"
    static let RECENT_SERVICE="get-recent-buy"
    static let SERVICE_LIKE="add-package-service-rate"
    static let ADD_SERVICE_COMMENT="add-service-comment"
    static let SERVICE_COMMENT_LIST="list-comment"
    
    //shop
    static let ADD_TO_CART="add-to-cart"
    static let GET_SHOPPING_CART="get-shopping-cart"
    static let GET_USER_ADDRESS="get-user-address"
    static let ADD_USER_ADDRESS="add-user-address"
    static let SELECT_INVOICE_ADDRESS="select-invoice-address"
    static let CHANGE_CART_QUANTITY="change-cart-quantity"
    static let GET_PAYMENT_METHOD="get-payment-methods"
    static let SELECT_PAYMENT_METHOD="select-payment-method"
    static let GET_PRODUCT_ATTR="get-product-attribute"
    static let SELECT_SHIPMENT_METHOD="select-shipment"
    static let GET_SHIPMENT_LIST="get-shipments"
    static let DELETE_CART_ITEM="delete-cart-content"
    static let COMPUTE_PRICE="compute-price"
    
    
    //CHAT
    static let LIST_MESSAGE="list-message"
    static let SEND_MESSAGE="send-message"
    
    
    //notification
    static let NOTIFICATION_COUNTER="get-counter"
    static let USER_NOTIFICATION_LIST="infoboard"
    static let READ_NOTIFICATION="read-message"
    
    //GALLARY
    static let IMAGE_GALLERY="get-gallary"
    
    //WALLET
    static let WALLET_CHECKOUT="wallet"
    static let WALLET_CHARGE_SETTING="wallet-charge-setting"
    static let WALLET_CHARGE="wallet-charge"
    
    //BPMS
    static let GET_WORKFLOW_DESKTOP="mytask"
    static let GET_WORKFLOW_TASK_LIST="workflow"
    
}
