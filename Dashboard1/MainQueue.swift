//
//  MainQueue.swift
//  AppSiteWebService
//
//  Created by Hasani on 2/7/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import Foundation
func performUIUpdatesOnMain(_ updates: @escaping () -> Void) {
    DispatchQueue.main.async {
        updates()
    }
}
