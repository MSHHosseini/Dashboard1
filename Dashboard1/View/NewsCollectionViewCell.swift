//
//  NewsCollectionViewCell.swift
//  Dashboard1
//
//  Created by Hasani on 3/15/1397 AP.
//  Copyright © 1397 MSH. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var newsLike: UILabel!
    @IBOutlet weak var newsImg: UIImageView!
    @IBOutlet weak var newsSummary: UILabel!
}
